from django.apps import AppConfig


class St7AppConfig(AppConfig):
    name = 'st7app'
