$(document).ready(function(){

var allPanels = $('.accordion > dd').hide();
var color = ["#3498db", "#2c3e50", "#e74c3c", "#95a5a6"];
var i = -1;
    
$('.accordion > dt > a').click(function() {
    allPanels.slideUp();

    if ($(this).parent().next().css('display') == 'none') {
        $(this).parent().next().slideDown();
    }else{
        $(this).parent().next().slideUp(); 
    }

    return false;
}); 

$("button").click(function(){
    i = i < color.length-1 ?  ++i : 0;
    $("body").css({"background" : color[i]});
    if(i%2 == 0){
        $("h2").css({"color" : "black"});
    }
    if(i%2 == 1){
        $("h2").css({"color" : "red"});
    }
});



});

