$(document).ready(function(){

var allPanels = $('.accordion > dd').hide();
var color = ["linear-gradient(298.74deg, #FCDF8C 0%, rgba(255, 255, 255, 0) 100%), #F5887C", "linear-gradient(298.74deg, #698EE6 0%, rgba(255, 255, 255, 0) 100%), #F531C5", "linear-gradient(298.74deg, #FF807E 0%, rgba(255, 255, 255, 0) 100%), #FF4BD3", "linear-gradient(298.74deg, #1295A3 0%, rgba(255, 255, 255, 0) 100%), #3C1161"];
var i = -1;
    
$('.accordion > dt > a').click(function() {
    allPanels.slideUp();

    if ($(this).parent().next().css('display') == 'none') {
        $(this).parent().next().slideDown();
    }else{
        $(this).parent().next().slideUp(); 
    }

    return false;
}); 

$("button").click(function(){
    i = i < color.length-1 ?  ++i : 0;
    $("body").css({"background" : color[i]});
    if(i%2 == 0){
        $("h2").css({"color" : "#383838"});
        $("h6").css({"color" : "#383838"});
        $("a").css({"color" : "#383838"});
        $("dd").css({"color" : "#383838"});
        $("button").css({"color" : "#383838"});
    }
    if(i%2 == 1){
        $("h2").css({"color" : "white"});
        $("h6").css({"color" : "white"});
        $("a").css({"color" : "white"});
        $("dd").css({"color" : "white"});
        $("button").css({"color" : "white"});
    }
});



});

